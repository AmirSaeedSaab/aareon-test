# Aareon Angular Test #

There are so many possibilities to enhance or i could have done this by using 
Angular Materia, Bootstrap, SCSS or media Queries for responsiveness based on screen sizes for mobile, tablets and desktops. 

I kept it simple based on strictly requirements and lack of time. 

To run this project, you need 

### Setup ###

* Node 12 or over 
* Latest Angular Cli 

### How do I get set up? ###

* run: npm install
* run: ng serve

### Test completed by ###

* By Amir Saeed

### Screen Shots ###

![alt text](https://bitbucket.org/AmirSaeedSaab/aareon-test/raw/83fd87fbb636f511c39ac0ac69aaf376d681d04c/form.png)
![alt text](https://bitbucket.org/AmirSaeedSaab/aareon-test/raw/83fd87fbb636f511c39ac0ac69aaf376d681d04c/form2.png)
![alt text](https://bitbucket.org/AmirSaeedSaab/aareon-test/raw/fd3ff3d5938b68b104f4d499039e848d522ce50e/form3.png)
![alt text](https://bitbucket.org/AmirSaeedSaab/aareon-test/raw/e35274ff7d5de8c5eec929d008da8eaf58478bb1/form4.png)
