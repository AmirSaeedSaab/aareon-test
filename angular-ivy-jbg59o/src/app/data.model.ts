import { FormArray, FormGroup } from "@angular/forms";

export interface FormDataModel {
    acceptTerms: boolean;
    firstName: string;
    lastName: string;
    title: string;
}

export type FormModel<T> = { [P in keyof T]: [T[P], any?] | FormGroup | FormArray };
