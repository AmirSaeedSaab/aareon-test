import { Component, Input } from '@angular/core';

@Component({
  selector: 'control-messages',
  templateUrl: './control-messages.component.html',
  styleUrls: ['./control-messages.component.css']
})
export class ControlMessagesComponent {

  @Input()
  control!: any;

  @Input()
  textError?: string;

  constructor() { }

  get errorMessage(): string {
    if (this.control.errors.hasOwnProperty('required')) {
      return this.textError;
    }
    return '';
  }
}
