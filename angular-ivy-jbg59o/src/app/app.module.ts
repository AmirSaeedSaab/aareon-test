import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SimpleformComponent } from './simple-form.component';
import { ControlMessagesComponent } from './control-messages/control-messages.component';

@NgModule({
  imports:      [ BrowserModule, FormsModule, ReactiveFormsModule ],
  declarations: [ AppComponent, SimpleformComponent, ControlMessagesComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
