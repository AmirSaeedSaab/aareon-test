import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Title } from './title.model';
import { FormDataModel, FormModel } from './data.model';
import { TitleService } from './title.service';
import { FormBuilder, Validators } from '@angular/forms';
import { ErrorsList } from './messages.enum';

@Component({
  selector: 'simple-form',
  templateUrl: './simple-form.component.html',
  styleUrls: ['./simple-form.component.css']
})
export class SimpleformComponent implements OnInit {
  public form;
  constructor(
    private titleService: TitleService,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {

    const personFormConfig: FormModel<FormDataModel> = {
      firstName: [''],
      lastName: ['', Validators.required],
      title: [''],
      acceptTerms: [false]
    };
    this.form = this.formBuilder.group(personFormConfig);
  }

  public getTitles(): Observable<Title[]> {
    return (
      this.titleService.getTitles()
        .pipe(
          map((items: Title[]) => items.filter(item => item.name !== '!')),
          tap((results: Title[]) => results.sort((a, b) => (a.name < b.name) ? -1 : 1))
        )
    )
  }

  // convenience getter for easy access to form fields
  get f() { return this.form.controls; }

  public onSubmit(instance: FormDataModel) {
    this.form.markAllAsTouched();
    if (this.form.valid) {
      console.log('Form Date:', instance);
    }
  }

  public get errorsList(): typeof ErrorsList {
    return ErrorsList;
  }
}
